---
title: Solar Lamp distribution among Dalits 2016 - 17
subtitle: Provide light during night, and children can study in the night.
layout: blog
date: 2016-12-23
img: dreams.png
category: activity
description: Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.
---

Solar lamp distribution was organized on 23rd December 2016 in Samastipur district. More than 110 Lamps were distributed to backward community in the presence of govt. officials and local public.

Solar lamps will provide light without electricity supply from Govt. and
help young children with their studies during night.

---

Coverage: JITWARPUR NIZAMAT, JITWARPUR CHOUTH & LAGUNIA SURYAKANTH, KORBADHA

The above segment (teen gaccha, mushari) is situated at meeting point of 3 gram panchayats namely, around JITWARPUR NIZAMAT, JITWARPUR CHOUTH & LAGUNIA SURYAKANTH, KORBADHA Gram panchayat

Beneficiaries: Mahadalit / Minority

---

| GRAM PANCHAYATS    | WARD NO | FEMALES | HANDICAPPED | OTHER | TOTAL |
| ------------------ | ------- | ------- | ----------- | ----- | ----- |
| JITWARPUR NIZAMAT  | 13      | 3       | -           | 13    | 16    |
| JITWARPUR CHOUTH   | 8       | 22      | -           | 9     | 31    |
| JITWARPUR CHOUTH   | 13 & 14 | 35      | 4           | 13    | 52    |
| LAGUNIA SURYAKANTH | 6       | 4       | 1           | 9     | 14    |
| Total              |         | 64      | 5           | 44    | 113   |

---

### ABOUT TOLA/SEGMENT

- The population above 300. and all are unskilled / Agriculture labours (Maha dalit).

> Solar Lamps WERE DISTRIBUTED IN THE PRESENCE OF ELECTED
> REPRESENTATIVES AND OTHER ELITE PEOPLE OF THE THREE
> GRAMPANCHYATS & SURROUNDINGS.

### Photo Gallery

<div class="row">
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture1.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture2.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture3.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture4.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture5.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture6.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-12">
    <img src="/img/activities/2016-12/Picture7.jpg" class="img-responsive img-rounded" />
  </div>

</div>

<div class="row">
  <div class="col-md-12">
    <h3>Power Point presentation of the event</h3>
    <iframe src="https://docs.google.com/presentation/d/1y13gU8X2wZ_SmTXAlTheFmdB30iMoC5tXeNoQH-z6WM/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="749" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
  </div>
</div>
