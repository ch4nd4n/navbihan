---
layout: blog
title: ACTIVITY 7 RURAL WOMAN LITERACY PROGRAMME 2018 - 19
date: 2020-09-03T00:00:00.000Z
subtitle: 70 Women were supplied with slates, Chalks, Books
description: 70 Women were supplied with slates, Chalks, Books
category: activities
---
Total 70 Women were supplied with slates, Chalks, Books individually &amp; were taught by Trainers.

Training Period – 2 Months (All trainees’ women (70) and trainers (5) were rewarded suitably by way
of Certificate of Appreciation.

| Details        |                                                                                                                                                                                                                         |
| -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Date:          | 31 st march 2019.                                                                                                                                                                                                       |
| Beneficiaries: | Illiterate Women                                                                                                                                                                                                        |
| Class:         | Mahadalit (Schedule caste- Musahar community)                                                                                                                                                                           |
| Coverage:      | TINGACHHA MUSHARI<br/> Ward No. 13, Jitwarpur Nizamat Grampanchayat, Samastipur<br/> Ward No. 6, Lagunia Suryakanth, Gram Panchayat (Korbaddha) Samastipur<br /> Ward No. 8 Jitwarpur Chauth Gram Panchayat, Samastipur |

- - -

| Gram Panchayat                 | Ward No | No. of Successful Trainees | No. of Trainers |
| ------------------------------ | ------- | -------------------------- | --------------- |
| Jitwarpur Nizamat              | 13      | 17                         | 2               |
| Jitwarpur Chauth               | 8       | 26                         | 2               |
| Lagunia Suryakanth (Korbaddha) | 6       | 12                         | 1               |
| Total                          | 3       | 55                         | 5               |

> SPONSOR
> RITA SMARAK NIDHI (RITA MEMORIAL FUND)
> WARD NO. 13 (BULLECHAK)
> JITWARPUR NIZAMAT, DISTRICT – SAMASTIPUR – 848134