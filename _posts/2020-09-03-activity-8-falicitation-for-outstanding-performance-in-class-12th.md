---
layout: blog
title: "Activity 8: Facilitation for outstanding performance in class 12th"
date: 2020-09-03T01:00:00.00Z
category: activity
subtitle: Facilitation for outstanding performance in class 12th
description: Facilitation for outstanding performance in class 12th
---
Facilitation for outstanding performance in class 12th

| Details            |                                            |
| ------------------ | ------------------------------------------ |
| Name               | Mr. Kunal Kumar                            |
| Father’s name      | Pawan Kumar                                |
| Date               | 31-03/2019                                 |
| Mother’s Name      | Mrs. Baby Devi                             |
| Grandfather’s name | Late Shree Ramashish Ray                   |
| Address            | Jitwarpur Nizamat, Ward No. 13 Samastipur  |
| Marks Secured      | 86.20 % (431 out of 500)                   |
| College            | Samastipur College, Samastipur             |
| Board              | Bihar Intermediate Education Council Patna |

Mr. Kunal was rewarded with Rs. 5000/- through cheque for his outstanding achievement in academics.

### SPONSOR

RITA SMARAK NIDHI (RITA MEMORIAL FUND)
WARD NO. 13 (BULLECHAK)
JITWARPUR NIZAMAT, DISTRICT – SAMASTIPUR – 848134
