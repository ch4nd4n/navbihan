---
layout: blog
title: Navbihan rural women literacy program 2019-20
date: 2020-11-01T18:26:25.163Z
subtitle: Navbihan rural women literacy program 2019-20
description: Navbihan rural women literacy program 2019-20
category: blog
---

Training Period – 2 Months All trainees women (92), trainers (16) & Supervisors (2) were rewarded by way of Certificate of Appreciation and token useful gifts. Also 12 girls of Ward No. 13 who passed 10th / 12th / Graduation / Post Graduation during 2017-18 also were rewarded by Certificate of Appreciation & Cash Award by Cheque.

| Date                       | 6th August 2019                                                                                          |
| -------------------------- | -------------------------------------------------------------------------------------------------------- |
| Place                      | Shri Ramdeo Rai residence,ward no.2 Jitwarpur Nizamat, Samastipur – 848134.                              |
| Beneficiaries              | Illiterate Women                                                                                         |
| Class                      | OBC/scheduled Castes                                                                                     |
| Coverage                   | Ward No. 1 & 2 Jitwarpur Nizamat Grampanchayat, Samastipur                                               |
|                            | Ward. 1 Female: 185 male: 245. Total. 430                                                                |
|                            | Ward 2. Female: 245, male: 260. Total. 505                                                               |
| Total population           | 1600 approx.                                                                                             |
| Chief Guest                | Smt. Prem lata (Chairman, Zila Parishad, Samastipur dist)                                                |
| Special Guest              | Smt Meruna Devi Ex.Block Pramukh, Samastipur Block                                                       |
| Guest of honour            | Smt. Shalini Devi, Mukhia, Jitwarpur Nizamat gram Panchayat                                              |
| Presence                   | Local Elected Representatives, Senior Citizen, Social Activists, Villagers with a large number of women. |
| No. of successful trainees | 90 Women                                                                                                 |
| Trainers/volunteers        | 8                                                                                                        |

> SPONSOR
> RITA SMARAK NIDHI (RITA MEMORIAL FUND)
> Ward no 13
> JITWARPUR NIZAMAT, DISTRICT – SAMASTIPUR – 848134.
