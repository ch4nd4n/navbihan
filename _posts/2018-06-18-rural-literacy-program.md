---
layout: blog
title: RURAL WOMAN LITERACY PROGRAMME 2018-19
date: 2018-06-18
subtitle: Small step to bring women to main stream.
description: Small step to bring women to main stream.
category: activity
---

All Woman (120) were supplied with slates, Chalks, Books individually & were taught by
Volunteers.

Training Period – 2 Months
All trainees women (92), trainers (16) & Supervisors (2) were rewarded by way of Certificate of
Appreciation and token useful gifts. Also 12 girls of Ward No. 13 who passed 10th / 12th /
Graduation / Post Graduation during 2017-18 also were rewarded by Certificate of Appreciation
& Cash Award by Cheque.

| | |
|-|-|
| Date | 6th August 2018 |
| Place | Society Office, Naina Sevak Kutir, Ward No. 13, (Bullechak) Jitwarpur Nizamat, Samastipur – 848134. |
| Beneficiaries | Illiterate Women |
| Class | OBC |
| Coverage | Ward No. 13, Jitwarpur Nizamat Grampanchayat, Samastipur |
| Chief Guest | Smt. Premlata (Chairman, Zila Parishad, Samastipur) |
| Special Guest | Smt. Poonam Kumari, Director District Rural Development Agency (DRDA, Samastipur District) |
| Guest of honour | Smt. Meruna Devi, Ex-Block Pramukh, Samastipur Block |
| Presence | Local Elected Representatives, Senior Citizen, Social Activists, Villagers with large number of women.|
| No. of successful trainees | 92 Women |

### Sponsor

> RITA SMARAK NIDHI (RITA MEMORIAL FUND)
>
> WARD NO. 13 (BULLECHAK)
>
> JITWARPUR NIZAMAT, DISTRICT – SAMASTIPUR – 848134.
