---
title: Woolen blanket distribution among marginalized poor people 2014 - 15
subtitle: To protect them from severe cold during winter
layout: blog
date: 2015-01-26
img: dreams.png
category: activity
description: Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.
---

<img src="/img/activities/2015-01/main.jpg" class="img-responsive img-rounded" />

###### To protect them from severe cold during winter

Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.

---

Coverage: tingachha musahar tola/segment

Gram panchayats: jitwarpur nizamat, jitwarpur chouth & lagunia suryakanth, korbadha

Beneficiaries: mahadalit / musahar communities

---

| GRAM PANCHAYATS    | WARD NO | WIDOWS | SR. CITIZENS | HANDI-CAPPED | TOTAL |
| ------------------ | ------- | ------ | ------------ | ------------ | ----- |
| JITWARPUR NIZAMAT  | 13      | 1      | 5            | -            | 6     |
| JITWARPUR CHOUTH   | 8       | 5      | 2            | 1            | 8     |
| LAGUNIA SURYAKANTH | 6       | 7      | 2            | 2            | 11    |
| TOTAL              |         | 13     | 9            | 3            | 25    |

### ABOUT TOLA/SEGMENT

- The above tola is situated at remote isolated place and on margin of 3 gram panchayats.
- The population above 200. all are unskilled / agriculture labours.
- Entire population above 18 years are illiterate.
- Nobody has reached even 9th standard of education.
- No LPG connection & no individual electric connection.
- No public toilet facility.
- No motorbike owned by them.
- None except one has got govt. employment
- A few of them own mobile phones.

> BLANKETS WERE DISTRIBUTED IN THE PRESENCE
> OF ELECTED REPRESENTETIVES AND OTHER ELITE PEOPLE
> OF THE THREE GRAMPANCHYATS & SURROUNDINGS.

### Photo Gallery

<div class="row">
  <div class="col-md-6">
    <img src="/img/activities/2015-01/2.jpg" alt="Blanket Distribution" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2015-01/3.jpg" alt="Blanket Distribution" class="img-responsive img-rounded"/>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <img src="/img/activities/2015-01/4.jpg" alt="Blanket Distribution" class="img-responsive img-rounded"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2015-01/5.jpg" alt="Blanket Distribution" class="img-responsive img-rounded"/>
  </div>
</div>
