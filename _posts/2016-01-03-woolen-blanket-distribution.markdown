---
title: Woolen blanket distribution 2015 - 16
subtitle: To protect them from severe cold during winter
layout: blog
date: 2016-01-03
img: dreams.png
category: activity
description: Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.
---

<img src="/img/activities/2016-01-03/main.jpg" class="img-responsive img-rounded" />

###### To protect them from severe cold during winter

Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.

---

Coverage: PREM KHIRAHAR POKHAR

Gram panchayat: jitwarpur Chouth

Beneficiaries: Mahadalit / Minority

---

| GRAM PANCHAYATS  | WARD NO | WIDOWS | SR. CITIZENS | HANDI-CAPPED | TOTAL |
| ---------------- | ------- | ------ | ------------ | ------------ | ----- |
| JITWARPUR CHOUTH | 7       | 7      | 1            | 2            | 10    |
| JITWARPUR CHOUTH | 8       | 6      | 6            | 2            | 14    |
| JITWARPUR CHOUTH | 9       | 1      | -            | -            | 1     |
| TOTAL            |         | 14     | 7            | 4            | 25    |

### ABOUT TOLA/SEGMENT

- The above segment is situated at in & around Khirhar pokhar in Jitwarpur chouth gp.
- The population above 300. and all are unskilled / Agriculture labours.

> BLANKETS WERE DISTRIBUTED IN THE PRESENCE
> OF ELECTED REPRESENTATIVES AND OTHER ELITE PEOPLE
> OF THE THREE GRAMPANCHYATS & SURROUNDINGS.

### Photo Gallery

<div class="row">
  <div class="col-md-6">
    <img src="/img/activities/2016-01-03/1.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2016-01-03/2.jpg" class="img-responsive"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2016-01-03/3.jpg" class="img-responsive"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2016-01-03/4.jpg" class="img-responsive"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2016-01-03/5.jpg" class="img-responsive"/>
  </div>
</div>
