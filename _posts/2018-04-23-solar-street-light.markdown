---
title: Solar Street Light Installation 2017 - 18
subtitle: Lighting up village road
layout: blog
date: 2018-04-23
category: activity
description: Installation of Solar Street lights in Jitwarpur
---

<img src="/img/activities/2018/NB-1-IMG-20180408-WA0086.jpg" class="img-responsive img-rounded" />

---

In Jitwarpur, Navbihan Sewa Society carried out installation of Solar Street lights on 25th of March 2018.

---

<table class="table table-bordered">
  <caption>Location and specification of solar lights installed</caption>
  <tbody>
    <tr>
      <td>
        LOCATION
      </td>
      <td>
        WARD 13 JITWARPUR NIZAMAT , SAMASTIPUR DIST (BIHAR) 848134
      </td>
    </tr>
    <tr>
      <td>
        NEAREST RAIL STATION
      </td>
      <td>
        SAMASTIPUR JN(SPJ)
      </td>
    </tr>
    <tr>
      <td>COVERAGE AREA
      </td>
      <td>
        1. JITWARPUR NIZAMAT GP (WARD 13)- 11 POLES
        2. JITWARPUR CHOUTH GP(WARD 8 ) - 2 POLES
        3. LAGUNIA SURJKANTH(WARD 5) GP- 2POLES
      </td>
    </tr>
    <tr>
      <td>POPULATION COVERAGE
      </td>
      <td>1000 APROX.
      </td>
    </tr>
    <tr>
      <td>ROAD DESCRIPTION
      </td>
      <td>INSTALATION DONE IN VILLAGE/GOVT PACCA ROAD UNCOVERED BY ELECTRIC LIGHT
      </td>
    </tr>
    <tr>
      <td>POLES
      </td>
      <td>15
      </td>
    </tr>
    <tr>
      <td>HEIGHT
      </td>
      <td>5 METRES OF EACH POLE
      </td>
    </tr>
    <tr>
      <td>PV MODULE/BATTERIES
      </td>
      <td>40 W, 12 V, 40 AH
      </td>
    </tr>
    <tr>
      <td>COMMISSIONING DATE
      </td>
      <td>25MARCH 2018
      </td>
    </tr>
    <tr>
      <td>ROAD COVERAGE
      </td>
      <td>1.5.KM APPROX
      </td>
    </tr>
  </tbody>
</table>

### Photo Gallery

<div class="row">
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-2-IMG-20180408-WA0078.jpg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-3-IMG-20180408-WA0061.jpg" class="img-responsive img-rounded"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-4-IMG-20180408-WA0062.jpg" class="img-responsive img-rounded"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-5-IMG-20180408-WA0074.jpg" class="img-responsive img-rounded"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-6-IMG-20180408-WA0068.jpg" class="img-responsive img-rounded"/>
  </div>
  <div class="col-md-6">
    <img src="/img/activities/2018/NB-7-IMG-20180408-WA0072.jpg" class="img-responsive img-rounded"/>
  </div>
</div>

### News Paper

Event coverage news clips

<div class="row">
  <div class="col-md-6">
    <img src="/docs/nb-newspaper-5-1.jpeg" class="img-responsive img-rounded" />
  </div>
  <div class="col-md-6">
    <img src="/docs/nb-newspaper-activity-5.jpeg" class="img-responsive img-rounded"/>
  </div>
</div>
