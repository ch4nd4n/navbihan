---
title: Woolen blanket distribution 2017 - 18
subtitle: To protect them from severe cold during winter
layout: blog
date: 2017-11-23
category: activity
description: Woolen blankets are minimal mean of protection during winter. This activity was to enable poor people to sustain themselves during extreme climate.
---

Woolen blanket distributed amongst widows, handicapped and senior citizens at Jitwarpur Railway Middle School

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/ck-/albums/72157700928400564" title="Navbihan: Activity 4"><img src="https://farm2.staticflickr.com/1876/30742668658_3708290223_c.jpg" width="800" height="600" alt="Navbihan: Activity 4"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

### Activity detail

| PLACE | JITWARPUR RAILWAY MIDDLE SCHOOL |
| BENEFICIARIES | WIDOWS, HANDICAPPED, AND SENIOR CITIZEN |
| CLASS | DALITS, MAHADALIT AND MOBC |
| COVERAGE | JITWARPUR NIZAMAT GRAM PANCHAYAT, SAMASTIPUR BLOCK |
| CHIEF GUEST | DR BHUVNESH MISHRA, BDO SAMASTIPUR BLOCK |
| GUEST OF HONOUR | SHRI BHARAT RAI, ZILA PARSHAD, SAMASTIPUR DIST SHRI SANJEEV KUMAR RAI, ZILA PARSHAD, SAMASTIPUR DIST |
| PRESENCE | LOCAL ELECTED REPRESENTATIVES, SENIOR CITIZEN, SOCIAL ACTIVISTS , VILLAGERS |

### Beneficiaries detail

| Ward NO | WIDOW | HANDICAPPED | SENIOR CITIZEN | TOTAL |
| ------- | ----- | ----------- | -------------- | ----- |
| 1       | 14    | 5           | 5              | 24    |
| 2       | 24    | 4           | 15             | 43    |
| 3       | 16    | 4           | 2              | 22    |
| 4       | 6     | 0           | 6              | 12    |
| others  | 1     | 2           | 1              | 4     |
| total   | 61    | 15          | 29             | 105   |
