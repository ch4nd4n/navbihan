---
layout: blog
title: "Activity 9: Navbihaan Sewa Society Organizes Successful Blood Donation and Health Checkup Camp"
date: 2023-04-25T01:00:00.00Z
category: activity
subtitle: Navbihaan Sewa Society organized a blood donation camp and general health checkup at their premises in April 2023, with 34 donors and over 100 participants. The event was supported by the Indian Red Cross Society, Samastipur, and facilitated by a book written by Dr. Atul Gawande. The Mayor of Samastipur Nagar Nigam inaugurated the event.
description: Navbihaan Sewa Society organized a blood donation camp and general health checkup at their premises in April 2023, with 34 donors and over 100 participants. The event was supported by the Indian Red Cross Society, Samastipur, and facilitated by a book written by Dr. Atul Gawande. The Mayor of Samastipur Nagar Nigam inaugurated the event.
---

We at Navbihaan Sewa Society recently organized a blood donation camp coupled with a general health checkup. The event was held on 17th April 2023 at our premises and was a great success.

We had an impressive turnout, with 34 Raktaveers (blood donors), including one lady, donating blood. Four medical professionals consisting of MBBS, MD, and qualified homeopath were also among the donors. The collected blood will be used for various medical purposes, including surgeries, accidents, and other emergencies.

In addition to the blood donation, we provided over 100 people, including a good number of women, with free health checkups. The health checkup involved tests for blood pressure, blood sugar, and other basic health parameters, with the aim of detecting any underlying health issues early on and providing timely treatment.

The event was supported by the Indian Red Cross Society, Samastipur, a well-known humanitarian organization that provides emergency assistance, disaster relief, and education across India. Their support added credibility and reach to the event.

We also facilitated doctors with a book named "Being Mortal: Medicine and What Matters in the End" written by Doctor Atul Gawande, a professor at Harvard Medical School, and a surgeon at Brigham and Women's Hospital in Boston. The book provided insights into the healthcare industry and the importance of providing compassionate care to patients. It also motivated the doctors to perform their duties with dedication and professionalism.

We provided complimentary letter pads to the donors and participants, which had our society's name and logo imprinted on them. This helped in creating awareness about our society and its activities.

The function was inaugurated by Smt. Anita Ram, the Mayor of Samastipur Nagar Nigam, who praised our efforts in organizing such events and encouraged people to participate in similar activities in the future.

We believe that events like these can create a positive impact on society by promoting health and well-being. We are grateful to everyone who participated and supported us in organizing this event. It serves as a reminder of the importance of blood donation and regular health checkups, and demonstrates the positive impact that NGOs can have on society.
