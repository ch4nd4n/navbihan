---
layout: blog
title: "Navbihan Activity Summary Update 2023"
date: 2023-06-19T01:00:00.00Z
category: activity
subtitle: "Navbihan Sewa Society: Making a Difference in the Lives of People"
description: Activity Summary update 2023
---

Navbihan Sewa Society is a non-profit organization that has been working in the field of social welfare for over 8 years. During this time, we have had the privilege of helping people in need.

## Our Activities

- **Woolen Blanket Distribution:** We distribute woolen blankets to poor and needy people during the winter months.
- **Solar Light Distribution:** We distribute solar lights to rural areas. Solar lights provide a source of clean and renewable energy, which is essential for people who live in remote areas.
- **Rural Women Literacy Program:** We run a literacy program for rural women. This program helps women learn basic literacy and numeracy skills, which can improve their lives in many ways.
- **Facilitating Girls for Passing Xth, XIIth, Graduations, etc.:** We provide financial assistance and other support to girls from underprivileged backgrounds to help them complete their education.
- **Special Activities:** We also organize various special activities, such as health camps, blood donation drives, and awareness campaigns. These activities help us to raise awareness about important social issues and to promote social change.

## Our Impact

We are proud of the work that we have done over the past 8 years, and we are committed to continuing our work in the future. We believe that everyone deserves to have a chance to live a happy and fulfilling life, and we are committed to helping make that happen.

## Our Future Plans

We plan to continue our work in the field of social welfare. We plan to expand our activities and reach out to more people in need. We also plan to raise awareness about social issues and promote social change.

To work with children between age of 3 to 6 years on the pattern of NEC 2020/ECCE of center/ state govt. program at termed as foundational stage.

## Activities

### Woolen Blanket Distribution

| Year    | Area                           | Beneficiaries | Class                       | Coverage                              |
| ------- | ------------------------------ | ------------- | --------------------------- | ------------------------------------- |
| 2014-15 | Tingachcha(entire area)        | 25            | Mahadalit                   | Senior Citizens, Widows, Handi-capped |
| 2015-16 | Khirhar Pokhar(Chouth GP)      | 25            | Mahadalit, Dalit, Minority  | Senior Citizens, Widows, Handi-capped |
| 2016-17 | Jitwarpur Nizamat WARD 1,2,3,4 | 125           | Mahadalit, Dalits, Most OBC | Senior Citizens, Widows, Handi-capped |

### Solar Light Distribution

| Year    | Areas                                 | Beneficiaries | Coverage                  |
| ------- | ------------------------------------- | ------------- | ------------------------- |
| 2016-17 | Total Tingachcha Pharpura Chouth      | 105           | Mahadalits                |
| 2017-18 | JITWARPUR Nizamat/ Chouth / Korbaddha | 15            | Street lights, Main Roads |

### Rural Women Literacy Program

| Year    | Areas             | Wards | Trainees no. | Trainers no. |
| ------- | ----------------- | ----- | ------------ | ------------ |
| 2018-19 | Jitwarpur Nizamat | 13    | 147          | 23           |
| 2019-20 | Jitwarpur Nizamat | 1, 2  | 52           | 8            |
| 2020-21 | Jitwarpur Nizamat | 3, 4  | 94           | 6            |
| 2021-22 | Jitwarpur Nizamat | 5, 10 | 33, 57       | 1, 4         |
| 2022-23 | Jitwarpur Nizamat | 6, 7  | 39           | 4            |

All trainees and trainers were given certificates of participation. All trainees were complimented with a cash award of Rs. 1,000 each through their bank accounts.

### GIRLS of WARD 13(now 19) of Jitwarpur Bullechak Nizamat facilitation

| Year  | Matric/Inter | Graduate | Ph.D. | Total |
| ----- | ------------ | -------- | ----- | ----- |
| 2018  | 11           | 1        | 0     | 12    |
| 2019  | 23           | 0        | 1     | 24    |
| 2020  | 18           | 0        | 0     | 18    |
| 2021  | 18           | 0        | 0     | 18    |
| 2022  | 19           | 1        | 1     | 21    |
| Total | 80           | 1        | 2     | 83    |

All successful girls in boards/universities were given a cash award of Rs. 1,000 each through their bank accounts, as well as a certificate of achievement.

## Special Program

**Kunal Kumar**

In 2019, Kunal Kumar from Ward 13 of Jitwarpur Nizamat was awarded a certificate of merit and a cash prize of Rs 5,000 for his outstanding performance in the Bihar board exams. Kunal scored 86.2% in the exams, which is a remarkable achievement.

**COVID-19 Support**

During the COVID-19 pandemic, Navbihan Sewa Society provided financial assistance to five daily wage earners from Ward 13 of Jitwarpur Nizamat. The society also distributed liquid disinfectant in and around the area to help prevent the spread of the virus. In addition, the society launched an awareness drive about COVID-19.

**Preeti Bharti**

In 2020, Preeti Bharti from Ward 12 of Jitwarpur Nizamat was awarded a cash prize and a certificate of appreciation for her outstanding performance in the CBSE Xth Board exams. Preeti scored 93.2% in the exams, which is a truly remarkable achievement.

**Sweta Kumari**

In 2022, Sweta Kumari from Ward 6 of Jitwarpur Nizamat was awarded a cash prize of Rs 5,000 and a certificate of appreciation for her outstanding performance in the Bihar Xth Board exams. Sweta scored 93.8% in the exams, which is a truly remarkable achievement.

Navbihan Sewa Society is committed to supporting the community in Jitwarpur Nizamat. The society's work is making a real difference in the lives of many people.

### Photographs

Following are some of the Photographs from various events organised

#### Solar Lamp Distribution

![Lamp Distribution](/img/activities/2023/navbihan-img-1.jpeg)

---

#### Blanket distribution

![Image of Woolen Blanket Distribution](/img/activities/2023/navbihan-img-2.jpeg)
![Image of Woolen Blanket Distribution](/img/activities/2023/navbihan-img-3.jpeg)
