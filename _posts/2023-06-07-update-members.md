---
layout: blog
title: "Navbihaan members 2023"
date: 2023-06-05T01:00:00.00Z
category: activity
subtitle: Navbihan Members in 2023
description: We introduce you to a dedicated group of individuals who are actively engaged in social work and making a positive impact on their community.
---

We introduce you to a dedicated group of individuals who are actively engaged in social work and making a
positive impact on their community. Let's meet the team members and learn about their designations,
educational backgrounds, and contact information.

### Navbihan Members

| Sl. No. | Name                                                    | Designation   | Education            | Mobile     |
| ------- | ------------------------------------------------------- | ------------- | -------------------- | ---------- |
| 1       | Sri Upendra Prasad Yadav<br>s/o Gurucharan Prasad Yadav | Chairman      | B.Sc                 | 9430046048 |
| 2       | Sri Jagadish Prasad Yadav<br>s/o Ganaur Yadav           | Vice Chairman | M.Sc                 | 9852150519 |
| 3       | Sri Chandreshwar Roy<br>s/o Bhauchan Roy                | Secretary     | M.Sc Ag.             | 9004316171 |
| 4       | Sri Ajay Kumar<br>s/o Ganaur Rai                        | Treasurer     | B.Sc.                | 8406832067 |
| 5       | Sri Ram Pratap Sada<br>s/o Ghuran Sada                  | Member        | Metric               | 7739075945 |
| 6       | Smt. Rita Kumar<br>d/o Amarjit Kumar                    | Member        | B.A B.Ed             | 8757729173 |
| 7       | Dr Sudhir Kumar<br>s/o Chandeshwar Prasad               | Member        | M.A, PhD             | 8757446666 |
| 8       | Sri. Anandi Rai<br>s/o Kuldeep Rai                      | Member        | Snatak               | 9931956504 |
| 9       | Sri. Hari Narayan Rai<br>s/o Nandhipat Rai              | Member        | M.Sc., B.Ed          | 9546647670 |
| 10      | Sri Hare Krishan Rajak<br>s/o Yogendra Rajak            | Member        | Diploma (Civil Eng.) | 9430923240 |
