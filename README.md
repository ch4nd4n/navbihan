# Agency Jekyll theme

Agency theme based on [Agency bootstrap theme ](http://startbootstrap.com/templates/agency/)

## Run it

```sh
bundle install # For the first time or updating Gemfile
bundle exec jekyll serve
```

# How to use

###Portfolio

Portfolio projects are in '/\_posts'

Images are in '/img/portfolio'

###About

Images are in '/img/about/'

###Team

Team members and info are in '\_config.yml'

Images are in '/img/team/'

# Demo

View this jekyll theme in action [here](https://y7kim.github.io/agency-jekyll-theme)

=========
For more details, read [documentation](http://jekyllrb.com/)

--

Activity 5 - Update content from yesterdays email
Activity 4 - Add spacing and border to table. Update formatting of above tabular. Add photos.
